﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using KuzniaMVP.Model;
using KuzniaMVP.Presenter;
using KuzniaMVP.View;
using Ninject;

namespace KuzniaMVP
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Ninject.IKernel kernel = new StandardKernel(new IoCModule());

      var mainForm = kernel.Get<MainForm>();
      var mainFormPresenter = kernel.Get<IMainFormPresenter>();

      Application.Run(mainForm);
    }
  }

  class IoCModule : Ninject.Modules.NinjectModule
  {
    public override void Load()
    {
      Bind<IMainFormPresenter>().To<MainFormPresenter>().InSingletonScope();
      Bind<IModel>().To<Model.Model>().InSingletonScope();
      Bind<MainForm, IMainFormView>().To<MainForm>().InSingletonScope();
    }
  }
}
