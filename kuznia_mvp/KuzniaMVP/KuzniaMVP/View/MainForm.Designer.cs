﻿namespace KuzniaMVP.View
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.SaveButton = new System.Windows.Forms.Button();
      this.RestoreButton = new System.Windows.Forms.Button();
      this.NextButton = new System.Windows.Forms.Button();
      this.PrevButton = new System.Windows.Forms.Button();
      this.AddBlankButton = new System.Windows.Forms.Button();
      this.RemoveButton = new System.Windows.Forms.Button();
      this.FirstNameTextBox = new System.Windows.Forms.TextBox();
      this.LastNameTextBox = new System.Windows.Forms.TextBox();
      this.StreetTextBox = new System.Windows.Forms.TextBox();
      this.CityTextBox = new System.Windows.Forms.TextBox();
      this.labelFirstName = new System.Windows.Forms.Label();
      this.labelLastName = new System.Windows.Forms.Label();
      this.labelStreet = new System.Windows.Forms.Label();
      this.labelCity = new System.Windows.Forms.Label();
      this.PersonGroupBox = new System.Windows.Forms.GroupBox();
      this.PersonGroupBox.SuspendLayout();
      this.SuspendLayout();
      // 
      // SaveButton
      // 
      this.SaveButton.Location = new System.Drawing.Point(142, 141);
      this.SaveButton.Name = "SaveButton";
      this.SaveButton.Size = new System.Drawing.Size(75, 23);
      this.SaveButton.TabIndex = 1;
      this.SaveButton.Text = "Save";
      this.SaveButton.UseVisualStyleBackColor = true;
      // 
      // RestoreButton
      // 
      this.RestoreButton.Location = new System.Drawing.Point(248, 141);
      this.RestoreButton.Name = "RestoreButton";
      this.RestoreButton.Size = new System.Drawing.Size(75, 23);
      this.RestoreButton.TabIndex = 2;
      this.RestoreButton.Text = "Restore";
      this.RestoreButton.UseVisualStyleBackColor = true;
      // 
      // NextButton
      // 
      this.NextButton.Location = new System.Drawing.Point(438, 272);
      this.NextButton.Name = "NextButton";
      this.NextButton.Size = new System.Drawing.Size(75, 23);
      this.NextButton.TabIndex = 3;
      this.NextButton.Text = "Next";
      this.NextButton.UseVisualStyleBackColor = true;
      // 
      // PrevButton
      // 
      this.PrevButton.Location = new System.Drawing.Point(357, 272);
      this.PrevButton.Name = "PrevButton";
      this.PrevButton.Size = new System.Drawing.Size(75, 23);
      this.PrevButton.TabIndex = 4;
      this.PrevButton.Text = "Prev";
      this.PrevButton.UseVisualStyleBackColor = true;
      // 
      // AddBlankButton
      // 
      this.AddBlankButton.Location = new System.Drawing.Point(357, 224);
      this.AddBlankButton.Name = "AddBlankButton";
      this.AddBlankButton.Size = new System.Drawing.Size(75, 23);
      this.AddBlankButton.TabIndex = 5;
      this.AddBlankButton.Text = "Add blank";
      this.AddBlankButton.UseVisualStyleBackColor = true;
      // 
      // RemoveButton
      // 
      this.RemoveButton.Location = new System.Drawing.Point(438, 224);
      this.RemoveButton.Name = "RemoveButton";
      this.RemoveButton.Size = new System.Drawing.Size(75, 23);
      this.RemoveButton.TabIndex = 6;
      this.RemoveButton.Text = "Remove";
      this.RemoveButton.UseVisualStyleBackColor = true;
      // 
      // FirstNameTextBox
      // 
      this.FirstNameTextBox.Location = new System.Drawing.Point(142, 19);
      this.FirstNameTextBox.Name = "FirstNameTextBox";
      this.FirstNameTextBox.Size = new System.Drawing.Size(181, 20);
      this.FirstNameTextBox.TabIndex = 7;
      // 
      // LastNameTextBox
      // 
      this.LastNameTextBox.Location = new System.Drawing.Point(142, 46);
      this.LastNameTextBox.Name = "LastNameTextBox";
      this.LastNameTextBox.Size = new System.Drawing.Size(181, 20);
      this.LastNameTextBox.TabIndex = 8;
      // 
      // StreetTextBox
      // 
      this.StreetTextBox.Location = new System.Drawing.Point(142, 73);
      this.StreetTextBox.Name = "StreetTextBox";
      this.StreetTextBox.Size = new System.Drawing.Size(181, 20);
      this.StreetTextBox.TabIndex = 9;
      // 
      // CityTextBox
      // 
      this.CityTextBox.Location = new System.Drawing.Point(142, 99);
      this.CityTextBox.Name = "CityTextBox";
      this.CityTextBox.Size = new System.Drawing.Size(181, 20);
      this.CityTextBox.TabIndex = 10;
      // 
      // labelFirstName
      // 
      this.labelFirstName.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.labelFirstName.AutoSize = true;
      this.labelFirstName.Location = new System.Drawing.Point(78, 22);
      this.labelFirstName.Name = "labelFirstName";
      this.labelFirstName.Size = new System.Drawing.Size(58, 13);
      this.labelFirstName.TabIndex = 11;
      this.labelFirstName.Text = "First name:";
      // 
      // labelLastName
      // 
      this.labelLastName.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.labelLastName.AutoSize = true;
      this.labelLastName.Location = new System.Drawing.Point(77, 49);
      this.labelLastName.Name = "labelLastName";
      this.labelLastName.Size = new System.Drawing.Size(59, 13);
      this.labelLastName.TabIndex = 12;
      this.labelLastName.Text = "Last name:";
      // 
      // labelStreet
      // 
      this.labelStreet.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.labelStreet.AutoSize = true;
      this.labelStreet.Location = new System.Drawing.Point(98, 76);
      this.labelStreet.Name = "labelStreet";
      this.labelStreet.Size = new System.Drawing.Size(38, 13);
      this.labelStreet.TabIndex = 13;
      this.labelStreet.Text = "Street:";
      // 
      // labelCity
      // 
      this.labelCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.labelCity.AutoSize = true;
      this.labelCity.Location = new System.Drawing.Point(109, 102);
      this.labelCity.Name = "labelCity";
      this.labelCity.Size = new System.Drawing.Size(27, 13);
      this.labelCity.TabIndex = 14;
      this.labelCity.Text = "City:";
      // 
      // PersonGroupBox
      // 
      this.PersonGroupBox.Controls.Add(this.CityTextBox);
      this.PersonGroupBox.Controls.Add(this.labelCity);
      this.PersonGroupBox.Controls.Add(this.FirstNameTextBox);
      this.PersonGroupBox.Controls.Add(this.labelStreet);
      this.PersonGroupBox.Controls.Add(this.LastNameTextBox);
      this.PersonGroupBox.Controls.Add(this.RestoreButton);
      this.PersonGroupBox.Controls.Add(this.labelLastName);
      this.PersonGroupBox.Controls.Add(this.SaveButton);
      this.PersonGroupBox.Controls.Add(this.StreetTextBox);
      this.PersonGroupBox.Controls.Add(this.labelFirstName);
      this.PersonGroupBox.Enabled = false;
      this.PersonGroupBox.Location = new System.Drawing.Point(184, 12);
      this.PersonGroupBox.Name = "PersonGroupBox";
      this.PersonGroupBox.Size = new System.Drawing.Size(329, 170);
      this.PersonGroupBox.TabIndex = 15;
      this.PersonGroupBox.TabStop = false;
      this.PersonGroupBox.Text = "Person";
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(582, 307);
      this.Controls.Add(this.PersonGroupBox);
      this.Controls.Add(this.RemoveButton);
      this.Controls.Add(this.AddBlankButton);
      this.Controls.Add(this.PrevButton);
      this.Controls.Add(this.NextButton);
      this.Name = "MainForm";
      this.Text = "Form1";
      this.PersonGroupBox.ResumeLayout(false);
      this.PersonGroupBox.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion
    private System.Windows.Forms.Button SaveButton;
    private System.Windows.Forms.Button RestoreButton;
    private System.Windows.Forms.Button NextButton;
    private System.Windows.Forms.Button PrevButton;
    private System.Windows.Forms.Button AddBlankButton;
    private System.Windows.Forms.Button RemoveButton;
    private System.Windows.Forms.TextBox FirstNameTextBox;
    private System.Windows.Forms.TextBox LastNameTextBox;
    private System.Windows.Forms.TextBox StreetTextBox;
    private System.Windows.Forms.TextBox CityTextBox;
    private System.Windows.Forms.Label labelFirstName;
    private System.Windows.Forms.Label labelLastName;
    private System.Windows.Forms.Label labelStreet;
    private System.Windows.Forms.Label labelCity;
    private System.Windows.Forms.GroupBox PersonGroupBox;
  }
}

