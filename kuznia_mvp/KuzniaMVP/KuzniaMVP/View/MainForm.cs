﻿using System;
using System.Windows.Forms;

namespace KuzniaMVP.View
{
  public partial class MainForm : Form, IMainFormView
  {
    public MainForm()
    {
      InitializeComponent();
      AttachEvents();
    }

    #region View -> Presenter

    public event EventHandler SaveButtonClick;
    public event EventHandler RestoreButtonClick;
    public event EventHandler NextButtonClick;
    public event EventHandler PreviousButtonClick;
    public event EventHandler AddBlankButtonClick;
    public event EventHandler RemoveButtonClick;

    #endregion

    #region Presenter -> View

    public void EnableEditing()
    {
      PersonGroupBox.Enabled = true;
    }

    public void DisableEditing()
    {
      PersonGroupBox.Enabled = false;
    }

    public void SetFirstName(string firstName)
    {
      FirstNameTextBox.Text = firstName;
    }

    public void SetLastName(string lastName)
    {
      LastNameTextBox.Text = lastName;
    }

    public void SetStreet(string street)
    {
      StreetTextBox.Text = street;
    }

    public void SetCity(string city)
    {
      CityTextBox.Text = city;
    }

    public string GetFirstName()
    {
      return FirstNameTextBox.Text;
    }

    public string GetLastName()
    {
      return LastNameTextBox.Text;
    }

    public string GetStreet()
    {
      return StreetTextBox.Text;
    }

    public string GetCity()
    {
      return CityTextBox.Text;
    }



    #endregion

    private void AttachEvents()
    {
      SaveButton.Click += (sender, args) => SaveButtonClick?.Invoke(this, args);
      RestoreButton.Click += (sender, args) => RestoreButtonClick?.Invoke(this, args);
      NextButton.Click += (sender, args) => NextButtonClick?.Invoke(this, args);
      PrevButton.Click += (sender, args) => PreviousButtonClick?.Invoke(this, args);
      AddBlankButton.Click += (sender, args) => AddBlankButtonClick?.Invoke(this, args);
      RemoveButton.Click += (sender, args) => RemoveButtonClick?.Invoke(this, args);
    }
  }
}
