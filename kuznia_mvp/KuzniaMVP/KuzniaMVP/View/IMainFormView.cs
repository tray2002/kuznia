using System;

namespace KuzniaMVP.View
{
  internal interface IMainFormView
  {
    event EventHandler SaveButtonClick;
    event EventHandler RestoreButtonClick;
    event EventHandler NextButtonClick;
    event EventHandler PreviousButtonClick;
    event EventHandler AddBlankButtonClick;
    event EventHandler RemoveButtonClick;
    
    void EnableEditing();
    void DisableEditing();
    void SetFirstName(string firstName);
    void SetLastName(string lastName);
    void SetStreet(string street);
    void SetCity(string city);

    string GetFirstName();
    string GetLastName();
    string GetStreet();
    string GetCity();
  }
}