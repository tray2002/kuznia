﻿using System;
using KuzniaMVP.Model;
using KuzniaMVP.View;

namespace KuzniaMVP.Presenter
{
  class MainFormPresenter : IMainFormPresenter, IPresenter<IMainFormView>
  {
    private IMainFormView _view;
    private IModel _model;

    public MainFormPresenter(IModel model, IMainFormView view)
    {
      _model = model;
      _view = view;
      AttachToModel();
      AttachToView();
    }

    private void AttachToModel()
    {
      _model.CurrentItemChanged += (sender, args) => RefreshView();
    }

    private void AttachToView()
    {
      _view.RestoreButtonClick += (o, args) => OnRestoreButtonClick();
      _view.SaveButtonClick += (o, args) => OnSaveButtonClick();
      _view.AddBlankButtonClick += (o, args) => OnAddBlankButtonClick();
      _view.NextButtonClick += (o, args) => OnNextButtonClick();
      _view.PreviousButtonClick += (o, args) => OnPreviousButtonClick();
      _view.RemoveButtonClick += (o, args) => OnRemoveButtonClick();
    }

    private void OnRemoveButtonClick()
    {
      _model.RemoveCurrent();
    }

    private void OnPreviousButtonClick()
    {
      _model.GoPrevious();
    }

    private void OnNextButtonClick()
    {
      _model.GoNext();
    }

    private void OnAddBlankButtonClick()
    {
      _model.AddBlank();
    }

    private void OnRestoreButtonClick()
    {
      RefreshView();
    }

    private void OnSaveButtonClick()
    {
      _model.CurrentItem.FirstName = _view.GetFirstName();
      _model.CurrentItem.LastName = _view.GetLastName();
      _model.CurrentItem.Street = _view.GetStreet();
      _model.CurrentItem.City = _view.GetCity();
    }

    private void RefreshView()
    {
      if (_model.EditEnabled)
      {
        _view.EnableEditing();
      }
      else
      {
        _view.DisableEditing();
      }

      var currentViewModel = _model.CurrentItem;

      _view.SetFirstName(currentViewModel?.FirstName ?? String.Empty);
      _view.SetLastName(currentViewModel?.LastName ?? String.Empty);
      _view.SetStreet(currentViewModel?.Street ?? String.Empty);
      _view.SetCity(currentViewModel?.City ?? String.Empty);
    }
  }
}
