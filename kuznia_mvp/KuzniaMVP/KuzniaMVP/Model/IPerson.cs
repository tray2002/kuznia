﻿namespace KuzniaMVP.Model
{
  internal interface IPerson
  {
    string FirstName { get; set; }
    string LastName { get; set; }
    string Street { get; set; }
    string City { get; set; }
  }
}