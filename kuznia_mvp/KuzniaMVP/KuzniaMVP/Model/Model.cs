using System;
using System.Collections.Generic;

namespace KuzniaMVP.Model
{
  class Model : IModel
  {
    private List<IPerson> _persons = new List<IPerson>();
    private int _currentItemIndex = -1;
    
    public IPerson CurrentItem => _persons.Count > 0 ? _persons[_currentItemIndex] : null;
    public bool EditEnabled => _persons.Count > 0;

    public event EventHandler CurrentItemChanged;


    public void AddBlank()
    {
      _persons.Add(new Person());
      _currentItemIndex = _persons.Count - 1;
      CurrentItemChanged?.Invoke(this, EventArgs.Empty);
    }

    public void RemoveCurrent()
    {
      if(_persons.Count > 0) { 
        _persons.RemoveAt(_currentItemIndex);
        _currentItemIndex = Math.Max(_currentItemIndex, _persons.Count - 1);
        CurrentItemChanged?.Invoke(this, EventArgs.Empty);
      }
    }

    public void GoNext()
    {
      _currentItemIndex = Math.Min(_currentItemIndex + 1, _persons.Count - 1);
      CurrentItemChanged?.Invoke(this, EventArgs.Empty);
    }

    public void GoPrevious()
    {
      _currentItemIndex = Math.Max(_currentItemIndex - 1, 0);
      CurrentItemChanged?.Invoke(this, EventArgs.Empty);
    }
  }
}