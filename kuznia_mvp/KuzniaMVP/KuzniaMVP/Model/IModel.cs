﻿using System;

namespace KuzniaMVP.Model
{
  interface IModel
  {
    IPerson CurrentItem { get; }
    bool EditEnabled { get; }

    event EventHandler CurrentItemChanged;

    void AddBlank();
    void GoNext();
    void GoPrevious();
    void RemoveCurrent();
  }
}