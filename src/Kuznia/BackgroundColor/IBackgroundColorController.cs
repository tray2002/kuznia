using System.ComponentModel;

namespace Kuznia.BackgroundColor
{
  internal interface IBackgroundColorController
  {
    BindingList<BackgroundColor> ListOfAllowedColors { get; }

    void SetColor(Kuznia.BackgroundColor.BackgroundColor newBackgroundColor);
  }
}