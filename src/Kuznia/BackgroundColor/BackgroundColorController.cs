using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using Kuznia.DataPersistance;

namespace Kuznia.BackgroundColor
{
  internal class BackgroundColorController : IBackgroundColorController
  {
    private static List<BackgroundColor> DefaultColors = new List<BackgroundColor>
    {
      new BackgroundColor() { Color = Color.Red, Name = "Czerwony" },
      new BackgroundColor() { Color = Color.Blue, Name = "Niebieski" },
      new BackgroundColor() { Color = Color.Green, Name = "Zielony" },
      new BackgroundColor() { Color = Color.Brown, Name = "Br�zowy" },
      new BackgroundColor() { Color = Color.Yellow, Name = "��ty" }
    };

    public BindingList<BackgroundColor> ListOfAllowedColors { get; } = new BindingList<BackgroundColor>(DefaultColors);

    private BackgroundColor _selectedColor;
    private readonly MainForm _window;
    private IApplicationDataPersister _applicationDataPersister;
    private const string KeyForPersistence = "BackgroundColor";

    public BackgroundColorController(MainForm windowToSetBackground, IApplicationDataPersister applicationDataPersister)
    {
      _selectedColor = ListOfAllowedColors[0];
      _window = windowToSetBackground;
      _applicationDataPersister = applicationDataPersister;
      RestoreColorFromAppSettings();
    }

    public void SetColor(BackgroundColor newBackgroundColor)
    {
      if (ListOfAllowedColors.IndexOf(newBackgroundColor) < 0)
      {
        throw new IndexOutOfRangeException();
      }
      _selectedColor = newBackgroundColor;
      StoreColorInAppSettings();
      SetNewColorForWindow();
    }

    private void StoreColorInAppSettings()
    {
      _applicationDataPersister.StoreStringValue(KeyForPersistence, _selectedColor.Name);
    }

    private void SetNewColorForWindow()
    {
      _window.BackColor = _selectedColor.Color;
    }

    private void RestoreColorFromAppSettings()
    {
      var appSettingsColorName = _applicationDataPersister.GetStringValue(KeyForPersistence);
      if (appSettingsColorName != null)
      {
        var colorOnList = ListOfAllowedColors.First(color => color.Name == appSettingsColorName);
        _selectedColor = colorOnList;
        SetNewColorForWindow();
      }
    }
  }
}