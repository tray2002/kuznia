using System.Drawing;

namespace Kuznia.BackgroundColor
{
  internal class BackgroundColor
  {
    public Color Color { get; set; }

    public string Name { get; set; }

    public override string ToString()
    {
      return Name;
    }
  }
}