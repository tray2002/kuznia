﻿namespace Kuznia
{
    public partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.MainFormMenuStrip = new System.Windows.Forms.MenuStrip();
      this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.ImportAlbumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.ExportAlbumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.ImagePreviewBox = new System.Windows.Forms.PictureBox();
      this.PreviousImageButton = new System.Windows.Forms.Button();
      this.NextImageButton = new System.Windows.Forms.Button();
      this.CommentForImageBox = new System.Windows.Forms.TextBox();
      this.WindowBackgroundColorSelector = new System.Windows.Forms.ComboBox();
      this.ChangeBackgroundColorButton = new System.Windows.Forms.Button();
      this.LoadNewPhotoButton = new System.Windows.Forms.Button();
      this.PhotosGroupBox = new System.Windows.Forms.GroupBox();
      this.RemovePhotoButton = new System.Windows.Forms.Button();
      this.PhotosListBox = new System.Windows.Forms.ListBox();
      this.FilterSetButton = new System.Windows.Forms.Button();
      this.FilterEnableCheck = new System.Windows.Forms.CheckBox();
      this.MainFormMenuStrip.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ImagePreviewBox)).BeginInit();
      this.PhotosGroupBox.SuspendLayout();
      this.SuspendLayout();
      // 
      // MainFormMenuStrip
      // 
      this.MainFormMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem,
            this.HelpToolStripMenuItem});
      this.MainFormMenuStrip.Location = new System.Drawing.Point(0, 0);
      this.MainFormMenuStrip.Name = "MainFormMenuStrip";
      this.MainFormMenuStrip.Size = new System.Drawing.Size(851, 24);
      this.MainFormMenuStrip.TabIndex = 0;
      this.MainFormMenuStrip.Text = "menuStrip1";
      // 
      // FileToolStripMenuItem
      // 
      this.FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportAlbumToolStripMenuItem,
            this.ExportAlbumToolStripMenuItem});
      this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
      this.FileToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
      this.FileToolStripMenuItem.Text = "Plik";
      // 
      // ImportAlbumToolStripMenuItem
      // 
      this.ImportAlbumToolStripMenuItem.Name = "ImportAlbumToolStripMenuItem";
      this.ImportAlbumToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
      this.ImportAlbumToolStripMenuItem.Text = "Importuj album";
      this.ImportAlbumToolStripMenuItem.Click += new System.EventHandler(this.ImportAlbumToolStripMenuItem_Click);
      // 
      // ExportAlbumToolStripMenuItem
      // 
      this.ExportAlbumToolStripMenuItem.Name = "ExportAlbumToolStripMenuItem";
      this.ExportAlbumToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
      this.ExportAlbumToolStripMenuItem.Text = "Eksportuj album";
      this.ExportAlbumToolStripMenuItem.Click += new System.EventHandler(this.ExportAlbumToolStripMenuItem_Click);
      // 
      // HelpToolStripMenuItem
      // 
      this.HelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AboutToolStripMenuItem});
      this.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem";
      this.HelpToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
      this.HelpToolStripMenuItem.Text = "Pomoc";
      // 
      // AboutToolStripMenuItem
      // 
      this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
      this.AboutToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
      this.AboutToolStripMenuItem.Text = "O aplikacji...";
      this.AboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
      // 
      // ImagePreviewBox
      // 
      this.ImagePreviewBox.Location = new System.Drawing.Point(12, 27);
      this.ImagePreviewBox.Name = "ImagePreviewBox";
      this.ImagePreviewBox.Size = new System.Drawing.Size(635, 577);
      this.ImagePreviewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.ImagePreviewBox.TabIndex = 1;
      this.ImagePreviewBox.TabStop = false;
      // 
      // PreviousImageButton
      // 
      this.PreviousImageButton.Enabled = false;
      this.PreviousImageButton.Location = new System.Drawing.Point(13, 611);
      this.PreviousImageButton.Name = "PreviousImageButton";
      this.PreviousImageButton.Size = new System.Drawing.Size(75, 23);
      this.PreviousImageButton.TabIndex = 2;
      this.PreviousImageButton.Text = "Poprzednie";
      this.PreviousImageButton.UseVisualStyleBackColor = true;
      this.PreviousImageButton.Click += new System.EventHandler(this.PreviousImageButton_Click);
      // 
      // NextImageButton
      // 
      this.NextImageButton.Enabled = false;
      this.NextImageButton.Location = new System.Drawing.Point(572, 611);
      this.NextImageButton.Name = "NextImageButton";
      this.NextImageButton.Size = new System.Drawing.Size(75, 23);
      this.NextImageButton.TabIndex = 3;
      this.NextImageButton.Text = "Następne";
      this.NextImageButton.UseVisualStyleBackColor = true;
      this.NextImageButton.Click += new System.EventHandler(this.NextImageButton_Click);
      // 
      // CommentForImageBox
      // 
      this.CommentForImageBox.Enabled = false;
      this.CommentForImageBox.Location = new System.Drawing.Point(94, 613);
      this.CommentForImageBox.Name = "CommentForImageBox";
      this.CommentForImageBox.Size = new System.Drawing.Size(472, 20);
      this.CommentForImageBox.TabIndex = 4;
      this.CommentForImageBox.Text = "Nie działa :(";
      // 
      // WindowBackgroundColorSelector
      // 
      this.WindowBackgroundColorSelector.FormattingEnabled = true;
      this.WindowBackgroundColorSelector.Location = new System.Drawing.Point(445, 639);
      this.WindowBackgroundColorSelector.Name = "WindowBackgroundColorSelector";
      this.WindowBackgroundColorSelector.Size = new System.Drawing.Size(121, 21);
      this.WindowBackgroundColorSelector.TabIndex = 5;
      // 
      // ChangeBackgroundColorButton
      // 
      this.ChangeBackgroundColorButton.Location = new System.Drawing.Point(572, 637);
      this.ChangeBackgroundColorButton.Name = "ChangeBackgroundColorButton";
      this.ChangeBackgroundColorButton.Size = new System.Drawing.Size(75, 23);
      this.ChangeBackgroundColorButton.TabIndex = 6;
      this.ChangeBackgroundColorButton.Text = "Zmień tło";
      this.ChangeBackgroundColorButton.UseVisualStyleBackColor = true;
      this.ChangeBackgroundColorButton.Click += new System.EventHandler(this.ChangeBackgroundColorButton_Click);
      // 
      // LoadNewPhotoButton
      // 
      this.LoadNewPhotoButton.Location = new System.Drawing.Point(7, 548);
      this.LoadNewPhotoButton.Name = "LoadNewPhotoButton";
      this.LoadNewPhotoButton.Size = new System.Drawing.Size(75, 23);
      this.LoadNewPhotoButton.TabIndex = 7;
      this.LoadNewPhotoButton.Text = "Załaduj";
      this.LoadNewPhotoButton.UseVisualStyleBackColor = true;
      this.LoadNewPhotoButton.Click += new System.EventHandler(this.LoadNewPhotoButton_Click);
      // 
      // PhotosGroupBox
      // 
      this.PhotosGroupBox.Controls.Add(this.FilterSetButton);
      this.PhotosGroupBox.Controls.Add(this.FilterEnableCheck);
      this.PhotosGroupBox.Controls.Add(this.RemovePhotoButton);
      this.PhotosGroupBox.Controls.Add(this.PhotosListBox);
      this.PhotosGroupBox.Controls.Add(this.LoadNewPhotoButton);
      this.PhotosGroupBox.Location = new System.Drawing.Point(653, 27);
      this.PhotosGroupBox.Name = "PhotosGroupBox";
      this.PhotosGroupBox.Size = new System.Drawing.Size(184, 577);
      this.PhotosGroupBox.TabIndex = 8;
      this.PhotosGroupBox.TabStop = false;
      this.PhotosGroupBox.Text = "Zdjęcia";
      // 
      // RemovePhotoButton
      // 
      this.RemovePhotoButton.Location = new System.Drawing.Point(103, 548);
      this.RemovePhotoButton.Name = "RemovePhotoButton";
      this.RemovePhotoButton.Size = new System.Drawing.Size(75, 23);
      this.RemovePhotoButton.TabIndex = 14;
      this.RemovePhotoButton.Text = "Usuń";
      this.RemovePhotoButton.UseVisualStyleBackColor = true;
      this.RemovePhotoButton.Click += new System.EventHandler(this.RemovePhotoButton_Click);
      // 
      // PhotosListBox
      // 
      this.PhotosListBox.FormattingEnabled = true;
      this.PhotosListBox.Location = new System.Drawing.Point(7, 44);
      this.PhotosListBox.Name = "PhotosListBox";
      this.PhotosListBox.Size = new System.Drawing.Size(171, 498);
      this.PhotosListBox.TabIndex = 0;
      this.PhotosListBox.SelectedIndexChanged += new System.EventHandler(this.PhotosListBox_SelectedIndexChanged);
      // 
      // FilterSetButton
      // 
      this.FilterSetButton.Location = new System.Drawing.Point(63, 15);
      this.FilterSetButton.Name = "FilterSetButton";
      this.FilterSetButton.Size = new System.Drawing.Size(75, 23);
      this.FilterSetButton.TabIndex = 12;
      this.FilterSetButton.Text = "Ustaw filtr";
      this.FilterSetButton.UseVisualStyleBackColor = true;
      this.FilterSetButton.Click += new System.EventHandler(this.FilterSetButton_Click);
      // 
      // FilterEnableCheck
      // 
      this.FilterEnableCheck.AutoSize = true;
      this.FilterEnableCheck.Location = new System.Drawing.Point(7, 19);
      this.FilterEnableCheck.Name = "FilterEnableCheck";
      this.FilterEnableCheck.Size = new System.Drawing.Size(50, 17);
      this.FilterEnableCheck.TabIndex = 13;
      this.FilterEnableCheck.Text = "Filtruj";
      this.FilterEnableCheck.UseVisualStyleBackColor = true;
      this.FilterEnableCheck.CheckedChanged += new System.EventHandler(this.FilterEnableCheck_CheckedChanged);
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(851, 681);
      this.Controls.Add(this.PhotosGroupBox);
      this.Controls.Add(this.ChangeBackgroundColorButton);
      this.Controls.Add(this.WindowBackgroundColorSelector);
      this.Controls.Add(this.CommentForImageBox);
      this.Controls.Add(this.NextImageButton);
      this.Controls.Add(this.PreviousImageButton);
      this.Controls.Add(this.ImagePreviewBox);
      this.Controls.Add(this.MainFormMenuStrip);
      this.MainMenuStrip = this.MainFormMenuStrip;
      this.Name = "MainForm";
      this.Text = "Steve";
      this.MainFormMenuStrip.ResumeLayout(false);
      this.MainFormMenuStrip.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ImagePreviewBox)).EndInit();
      this.PhotosGroupBox.ResumeLayout(false);
      this.PhotosGroupBox.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainFormMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportAlbumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExportAlbumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        private System.Windows.Forms.TextBox CommentForImageBox;
    private System.Windows.Forms.ComboBox WindowBackgroundColorSelector;
    private System.Windows.Forms.Button ChangeBackgroundColorButton;
    public System.Windows.Forms.PictureBox ImagePreviewBox;
    public System.Windows.Forms.Button PreviousImageButton;
    public System.Windows.Forms.Button NextImageButton;
    private System.Windows.Forms.Button LoadNewPhotoButton;
    private System.Windows.Forms.GroupBox PhotosGroupBox;
    private System.Windows.Forms.ListBox PhotosListBox;
    private System.Windows.Forms.Button FilterSetButton;
    private System.Windows.Forms.CheckBox FilterEnableCheck;
    private System.Windows.Forms.Button RemovePhotoButton;
  }
}

