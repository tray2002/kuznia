using System;
using System.Drawing;
using System.IO;

namespace Kuznia.ImageList
{
  public class ImageListElement
  {
    public ImageListElement()
    {
    }
    public ImageListElement(string fileName)
    {
      FileName = fileName;
    }

    public string FileName { get; set; }
    public string ShortName => Path.GetFileName(this.FileName);
    public DateTime CreationTime => File.GetCreationTime(this.FileName);
    public long Size => new FileInfo(this.FileName).Length;

    public Image LoadAsImage()
    {
      return Image.FromFile(this.FileName);
    }
  }
}