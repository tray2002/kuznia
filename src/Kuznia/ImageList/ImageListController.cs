using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Kuznia.Albums;

namespace Kuznia.ImageList
{
  public class ImageListController : IImageListController
  {
    public event OnImageChanged ImageChanged;

    private List<ImageListElement> _imageList = new List<ImageListElement>();
    public BindingList<ImageListElement> ImagesToShow { get; } = new BindingList<ImageListElement>();
    public IReadOnlyCollection<ImageListElement> AllPhotosOnAlbum => _imageList.AsReadOnly();

    private ImageListElement _currentImage;
    private FilterInfo _currentFilter = FilterInfo.Default;

    public bool IsFilterEnabled { get; private set; }

    public ImageListController()
    {
    }

    public void GoToNextImage()
    {
      var currentItemIndex = ImagesToShow.IndexOf(_currentImage);
      var nextIndex = Math.Min(currentItemIndex + 1, ImagesToShow.Count - 1);
      SetCurrentImage(ImagesToShow[nextIndex]);
    }

    public void GoToPreviousImage()
    {
      var currentItemIndex = ImagesToShow.IndexOf(_currentImage);
      var nextIndex = Math.Max(currentItemIndex - 1, 0);
      SetCurrentImage(ImagesToShow[nextIndex]);
    }

    public void AddImage(ImageListElement newImageListElement)
    {
      _imageList.Add(newImageListElement);
      ListChanged();
    }

    public void SetFilter(FilterInfo filter)
    {
      _currentFilter = filter;
    }

    public void EnableFilter()
    {
      IsFilterEnabled = true;
      ListChanged();
    }

    public void DisableFilter()
    {
      IsFilterEnabled = false;
      ListChanged();
    }

    public void RemovePhoto(ImageListElement itemToRemove)
    {
      _imageList.Remove(itemToRemove);
      ListChanged();
    }

    public void LoadPhotoAlbum(PhotoAlbum album)
    {
      DisableFilter();
      _imageList.Clear();
      _imageList.AddRange(album.Photos);
      ListChanged();
      SetCurrentImage(ImagesToShow[0]);
    }

    public void GoToImage(int index)
    {
      if (index >= ImagesToShow.Count) return;
      SetCurrentImage(ImagesToShow[index]);
    }

    private void ListChanged()
    {
      RecalculateImagesToShowList();
    }

    private void RecalculateImagesToShowList()
    {
      ImagesToShow.Clear();
      foreach (var imageFiltered in FilteredImages())
      {
        ImagesToShow.Add(imageFiltered);
      }
    }

    private IEnumerable<ImageListElement> FilteredImages()
    {
      return _imageList.Where(item => IsMatchFilter(item));
    }

    private bool IsMatchFilter(ImageListElement item)
    {
      if (IsFilterEnabled == false) return true;
      return MatchFilterCreationTime(item) && MatchFilterShortName(item);
    }

    private bool MatchFilterShortName(ImageListElement item)
    {
      if (_currentFilter.FilterByFileName == false) return true;
      return item.ShortName.Contains(_currentFilter.FileName);
    }

    private bool MatchFilterCreationTime(ImageListElement item)
    {
      if (_currentFilter.FilterByCreationDate == false) return true;
      return _currentFilter.CreationDate.Date == item.CreationTime.Date;
    }

    private void SetCurrentImage(ImageListElement image)
    {
      _currentImage = image;
      ImageChanged?.Invoke(this, new OnImageChangedEventArgs()
      {
        Element = image,
        IsLastElement = IsImageLastOnList(image),
        IsFirstElement = IsImageFirstOnList(image)
      });
    }

    private bool IsImageFirstOnList(ImageListElement element)
    {
      return ImagesToShow.IndexOf(element) <= 0;
    }

    private bool IsImageLastOnList(ImageListElement element)
    {
      return ImagesToShow.IndexOf(element) >= ImagesToShow.Count - 1 || ImagesToShow.IndexOf(element) < 0;
    }
  }
}