using System.Collections.Generic;
using System.ComponentModel;
using Kuznia.Albums;

namespace Kuznia.ImageList
{
  public interface IImageListController
  {
    BindingList<ImageListElement> ImagesToShow { get; }
    IReadOnlyCollection<ImageListElement> AllPhotosOnAlbum { get; }
    bool IsFilterEnabled { get; }
    void GoToNextImage();
    void GoToPreviousImage();
    void AddImage(ImageListElement newImageListElement);
    void SetFilter(FilterInfo filter);
    void EnableFilter();
    void DisableFilter();
    void RemovePhoto(ImageListElement itemToRemove);
    void LoadPhotoAlbum(PhotoAlbum album);
    void GoToImage(int index);
    
    event OnImageChanged ImageChanged;
  }
}