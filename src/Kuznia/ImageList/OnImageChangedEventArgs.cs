using System;

namespace Kuznia.ImageList
{
  public class OnImageChangedEventArgs : EventArgs
  {
    public ImageListElement Element { get; set; }

    public bool IsLastElement { get; set; }

    public bool IsFirstElement { get; set; }
  }
}