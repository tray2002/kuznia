﻿namespace Kuznia.DataPersistance
{
  internal interface IApplicationDataPersister
  {
    void StoreStringValue(string key, string value);
    string GetStringValue(string key);
  }
}