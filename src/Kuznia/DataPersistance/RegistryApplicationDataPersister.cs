using Microsoft.Win32;

namespace Kuznia.DataPersistance
{
  internal class RegistryApplicationDataPersister : IApplicationDataPersister
  {
    private const string _keyName = "KUZNIA\\Steve";
    private RegistryKey _registryKey = Registry.CurrentUser.CreateSubKey(_keyName);
    public void StoreStringValue(string key, string value)
    {
      _registryKey.SetValue(key, value);
    }

    public string GetStringValue(string key)
    {
      return (string)_registryKey.GetValue(key);
    }
  }
}