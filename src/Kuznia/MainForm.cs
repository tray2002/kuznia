﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kuznia.Albums;
using Kuznia.BackgroundColor;
using Kuznia.DataPersistance;
using Kuznia.ImageList;

namespace Kuznia
{
  public partial class MainForm : Form, IMainForm
  {
    private IImageListController _imageListController;
    private IBackgroundColorController _backgroundColorController;
    private IAlbumsManager _albumsManager;

    public MainForm()
    {
      InitializeComponent();
      this._imageListController = new ImageListController();
      this._backgroundColorController = new BackgroundColorController(this, new RegistryApplicationDataPersister());
      this._albumsManager = new AlbumsManager(new XmlAlbumSerializer(), _imageListController);

      this.WindowBackgroundColorSelector.DataSource = this._backgroundColorController.ListOfAllowedColors;
      this.PhotosListBox.DataSource = this._imageListController.ImagesToShow;
      this.PhotosListBox.DisplayMember = nameof(ImageListElement.ShortName);

      this._imageListController.ImageChanged += _imageListController_ImageChanged;
    }

    private void _imageListController_ImageChanged(object sender, OnImageChangedEventArgs eventArgs)
    {
      var image = eventArgs.Element.LoadAsImage();
      this.ImagePreviewBox.Image = image;
      this.PreviousImageButton.Enabled = eventArgs.IsFirstElement == false;
      this.NextImageButton.Enabled = eventArgs.IsLastElement == false;
      GC.Collect();
    }

    private void PreviousImageButton_Click(object sender, EventArgs e)
    {
      this._imageListController.GoToPreviousImage();
    }

    private void NextImageButton_Click(object sender, EventArgs e)
    {
      this._imageListController.GoToNextImage();
    }

    private void ChangeBackgroundColorButton_Click(object sender, EventArgs e)
    {
      var backgroundColorToSet = this.WindowBackgroundColorSelector.SelectedItem as BackgroundColor.BackgroundColor;

      this._backgroundColorController.SetColor(backgroundColorToSet);
    }

    private void LoadNewPhotoButton_Click(object sender, EventArgs e)
    {
      using (OpenFileDialog ofd = new OpenFileDialog())
      {
        if (ofd.ShowDialog() == DialogResult.OK)
        {
          var newImage = new ImageListElement(ofd.FileName);
          _imageListController.AddImage(newImage);
        }
      }
    }

    private void FilterSetButton_Click(object sender, EventArgs e)
    {
      using (FilterForm filterForm = new FilterForm())
      {
        if (filterForm.ShowDialog() == DialogResult.OK)
        {
          _imageListController.SetFilter(filterForm.FilterResult);
        }
      }
    }

    private void FilterEnableCheck_CheckedChanged(object sender, EventArgs e)
    {
      if (_imageListController.IsFilterEnabled)
      {
        _imageListController.DisableFilter();
      }
      else
      {
        _imageListController.EnableFilter();
      }
      FilterEnableCheck.Checked = _imageListController.IsFilterEnabled;
    }

    private void RemovePhotoButton_Click(object sender, EventArgs e)
    {
      var selectedPhoto = PhotosListBox.SelectedItem as ImageListElement;

      _imageListController.RemovePhoto(selectedPhoto);
    }

    private void ImportAlbumToolStripMenuItem_Click(object sender, EventArgs e)
    {
      _albumsManager.ImportAlbum();
    }

    private void ExportAlbumToolStripMenuItem_Click(object sender, EventArgs e)
    {
      _albumsManager.ExportAlbum();
    }

    private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      MessageBox.Show(this, "Autorem aplikacji jestem ja :)");
    }

    private void PhotosListBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (PhotosListBox.SelectedIndex < 0)
      {
        PhotosListBox.SelectedIndex = 0;
        return;
      }
      else
      {
        _imageListController.GoToImage(PhotosListBox.SelectedIndex);
      }
    }
  }

  public interface IMainForm
  {
  }
}
