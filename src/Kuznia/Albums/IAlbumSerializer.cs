﻿namespace Kuznia.Albums
{
  internal interface IAlbumSerializer
  {
    PhotoAlbum Deserialize(string fileName);
    void Serialize(string fileName, PhotoAlbum albumContent);
    string FileNameFilter { get; }
  }
}