﻿using System.Collections.Generic;
using Kuznia.ImageList;

namespace Kuznia.Albums
{
  public class PhotoAlbum
  {
    public List<ImageListElement> Photos { get; set; }
  }
}