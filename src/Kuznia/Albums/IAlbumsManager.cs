﻿namespace Kuznia.Albums
{
  internal interface IAlbumsManager
  {
    void ImportAlbum();
    void ExportAlbum();
  }
}