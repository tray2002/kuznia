﻿using System.Linq;
using System.Windows.Forms;
using Kuznia.ImageList;

namespace Kuznia.Albums
{
  internal class AlbumsManager : IAlbumsManager
  {
    private IAlbumSerializer _albumSerializer;
    private IImageListController _imageListController;

    public AlbumsManager(IAlbumSerializer albumSerializer, IImageListController imageListController)
    {
      _albumSerializer = albumSerializer;
      _imageListController = imageListController;
    }

    public void ImportAlbum()
    {
      using (OpenFileDialog openAlbumFileDialog = new OpenFileDialog())
      {
        openAlbumFileDialog.Filter = _albumSerializer.FileNameFilter;
        if (openAlbumFileDialog.ShowDialog() == DialogResult.OK)
        {
          var fileToImport = openAlbumFileDialog.FileName;
          PhotoAlbum albumContent = _albumSerializer.Deserialize(fileToImport);
          LoadAlbumContent(albumContent);
        }
      }
    }

    private void LoadAlbumContent(PhotoAlbum albumContent)
    {
      _imageListController.LoadPhotoAlbum(albumContent);
    }

    public void ExportAlbum()
    {
      using (SaveFileDialog saveAlbumFileDialog = new SaveFileDialog())
      {
        saveAlbumFileDialog.Filter = _albumSerializer.FileNameFilter;
        if (saveAlbumFileDialog.ShowDialog() == DialogResult.OK)
        {
          var fileToExport = saveAlbumFileDialog.FileName;
          var albumContent = GetCurrentAlbumContent();
          _albumSerializer.Serialize(fileToExport, albumContent);
        }
      }
    }

    private PhotoAlbum GetCurrentAlbumContent()
    {
      var allPhotosOnAlbum = _imageListController.AllPhotosOnAlbum;

      return new PhotoAlbum() { Photos = allPhotosOnAlbum.ToList() };
    }
  }
}