﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Kuznia.Albums
{
  class XmlAlbumSerializer : IAlbumSerializer
  {
    readonly XmlSerializer _xmlSerializer = new XmlSerializer(typeof(PhotoAlbum));

    public PhotoAlbum Deserialize(string fileName)
    {
      PhotoAlbum albumContent;

      using (var streamReader = new StreamReader(fileName))
      {
        albumContent = _xmlSerializer.Deserialize(streamReader) as PhotoAlbum;
      }

      return albumContent;
    }

    public void Serialize(string fileName, PhotoAlbum albumContent)
    {
      string xml = "";

      using (var stringWriter = new StringWriter())
      {
        using (var xmlWriter = XmlWriter.Create(stringWriter))
        {
          _xmlSerializer.Serialize(xmlWriter, albumContent);
          xml = stringWriter.ToString();
        }
      }

      using (var streamWriter = new StreamWriter(fileName))
      {
        streamWriter.Write(xml);
      }
    }

    public string FileNameFilter { get; } = "Xml|*.xml";
  }
}