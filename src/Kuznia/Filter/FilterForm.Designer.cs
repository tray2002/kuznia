﻿namespace Kuznia
{
  partial class FilterForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FilterCreationDatePicker = new System.Windows.Forms.DateTimePicker();
      this.FilterFileNameBox = new System.Windows.Forms.TextBox();
      this.FilterSetButton = new System.Windows.Forms.Button();
      this.FilterCreationDateEnabled = new System.Windows.Forms.CheckBox();
      this.FilterNameEnabled = new System.Windows.Forms.CheckBox();
      this.FilterCancelButton = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // FilterCreationDatePicker
      // 
      this.FilterCreationDatePicker.Location = new System.Drawing.Point(74, 36);
      this.FilterCreationDatePicker.Name = "FilterCreationDatePicker";
      this.FilterCreationDatePicker.Size = new System.Drawing.Size(198, 20);
      this.FilterCreationDatePicker.TabIndex = 9;
      // 
      // FilterFileNameBox
      // 
      this.FilterFileNameBox.Location = new System.Drawing.Point(74, 85);
      this.FilterFileNameBox.Name = "FilterFileNameBox";
      this.FilterFileNameBox.Size = new System.Drawing.Size(198, 20);
      this.FilterFileNameBox.TabIndex = 10;
      // 
      // FilterSetButton
      // 
      this.FilterSetButton.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.FilterSetButton.Location = new System.Drawing.Point(116, 135);
      this.FilterSetButton.Name = "FilterSetButton";
      this.FilterSetButton.Size = new System.Drawing.Size(75, 23);
      this.FilterSetButton.TabIndex = 13;
      this.FilterSetButton.Text = "Filtruj";
      this.FilterSetButton.UseVisualStyleBackColor = true;
      this.FilterSetButton.Click += new System.EventHandler(this.FilterSetButton_Click);
      // 
      // FilterCreationDateEnabled
      // 
      this.FilterCreationDateEnabled.AutoSize = true;
      this.FilterCreationDateEnabled.Location = new System.Drawing.Point(13, 13);
      this.FilterCreationDateEnabled.Name = "FilterCreationDateEnabled";
      this.FilterCreationDateEnabled.Size = new System.Drawing.Size(106, 17);
      this.FilterCreationDateEnabled.TabIndex = 14;
      this.FilterCreationDateEnabled.Text = "Data utworzenia:";
      this.FilterCreationDateEnabled.UseVisualStyleBackColor = true;
      // 
      // FilterNameEnabled
      // 
      this.FilterNameEnabled.AutoSize = true;
      this.FilterNameEnabled.Location = new System.Drawing.Point(13, 62);
      this.FilterNameEnabled.Name = "FilterNameEnabled";
      this.FilterNameEnabled.Size = new System.Drawing.Size(101, 17);
      this.FilterNameEnabled.TabIndex = 15;
      this.FilterNameEnabled.Text = "Nazwa zawiera:";
      this.FilterNameEnabled.UseVisualStyleBackColor = true;
      // 
      // FilterCancelButton
      // 
      this.FilterCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.FilterCancelButton.Location = new System.Drawing.Point(197, 135);
      this.FilterCancelButton.Name = "FilterCancelButton";
      this.FilterCancelButton.Size = new System.Drawing.Size(75, 23);
      this.FilterCancelButton.TabIndex = 16;
      this.FilterCancelButton.Text = "Anuluj";
      this.FilterCancelButton.UseVisualStyleBackColor = true;
      this.FilterCancelButton.Click += new System.EventHandler(this.FilterCancelButton_Click);
      // 
      // FilterForm
      // 
      this.AcceptButton = this.FilterSetButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.FilterCancelButton;
      this.ClientSize = new System.Drawing.Size(284, 170);
      this.ControlBox = false;
      this.Controls.Add(this.FilterCancelButton);
      this.Controls.Add(this.FilterNameEnabled);
      this.Controls.Add(this.FilterCreationDateEnabled);
      this.Controls.Add(this.FilterSetButton);
      this.Controls.Add(this.FilterCreationDatePicker);
      this.Controls.Add(this.FilterFileNameBox);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Name = "FilterForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Filtry";
      this.TopMost = true;
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DateTimePicker FilterCreationDatePicker;
    private System.Windows.Forms.TextBox FilterFileNameBox;
    private System.Windows.Forms.Button FilterSetButton;
    private System.Windows.Forms.CheckBox FilterCreationDateEnabled;
    private System.Windows.Forms.CheckBox FilterNameEnabled;
    private System.Windows.Forms.Button FilterCancelButton;
  }
}