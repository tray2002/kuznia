﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kuznia
{
  internal partial class FilterForm : Form
  {
    public FilterForm()
    {
      InitializeComponent();
    }

    public FilterInfo FilterResult { get; set; }

    private void FilterSetButton_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.FilterResult = new FilterInfo()
      {
        FilterByCreationDate = FilterCreationDateEnabled.Checked,
        CreationDate = FilterCreationDatePicker.Value,
        FilterByFileName = FilterNameEnabled.Checked,
        FileName = FilterFileNameBox.Text
      };
      this.Close();
    }

    private void FilterCancelButton_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
      this.Close();
    }
  }
}
