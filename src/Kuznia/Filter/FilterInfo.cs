using System;

namespace Kuznia
{
  public class FilterInfo
  {
    public DateTime CreationDate { get; set; }
    public string FileName { get; set; }
    public bool FilterByCreationDate { get; set; }
    public bool FilterByFileName { get; set; }

    public static FilterInfo Default => new FilterInfo
    {
      FilterByCreationDate = false,
      FilterByFileName = false
    };
  }
}