﻿using System;
using Kuznia.ImageList;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Moq;
using Assert = NUnit.Framework.Assert;

namespace Kuznia.Tests
{
  [TestFixture]
  public class UnitTest1
  {
    MockRepository _mockRepository = new MockRepository(MockBehavior.Default);

    [Test]
    public void TestMethod1()
    {
      var _sut = new ImageListController();
      bool isEventRaised = false;
      _sut.ImageChanged += (sender, eventArgs) => isEventRaised = true;
      _sut.AddImage(new ImageListElement("abc"));
      _sut.AddImage(new ImageListElement("efg"));

      //act
      _sut.GoToNextImage();

      //assert
      Assert.AreEqual(true, isEventRaised);
    }
  }
}
